#Written By Patrick Héas.                                          
#Copyright 2024 INRIA. All rights reserved  

################################################## Ref distribution & proposal kernel ################################################## 

mutable struct OrsteinUhlenbeckParams

    #parameters    
    alpha_prop::Float64 # stdev of O-Uhlenbeck
    pstar::Float64 # target acceptance probability
    c::Float64 # coefficient for stdev O-Uhlenbeck adaptation
    nMove::Int64 # number of kernel application
     # constructeur
    function OrsteinUhlenbeckParams(a_,p_,c_,n_)
        new(a_,p_,c_,n_)
    end
    
end

# reference distribution
abstract type AbstractRefDistrib end 

mutable struct logNormal <: AbstractRefDistrib

    #parameters ref law
    name::String
    paramDim::Int64 # dimension
    mu::Float64  
    sigma::Float64
    sigma_ref::Float64
    mu_ref::Float64

     # constructor
    function logNormal(p,m,s)
        new("logNormalLaw",p,m,s,sqrt(log(s^2/m^2 + 1)),log(m^2 / sqrt(s^2+m^2)))
    end
    
end

# lognormal sampler 
function refSampler(refDistrib::logNormal,N_samples,rng)

    return exp.(refDistrib.mu_ref .+ refDistrib.sigma_ref .* randn(rng,Float64,(N_samples,refDistrib.paramDim)))
    
end

################################################## FOMs ################################################## 

abstract type AbstractFOM end 

#1D analytical model

mutable struct Fom1D <: AbstractFOM

    L::Float64
    p1::Float64
    p2::Float64
    p3::Float64
    nbFOMcalls::Int64

    # constructeurs par défaut
    function Fom1D(L_,p1_,p2_,p3_)
        new(L_,p1_,p2_,p3_,0)
    end
    
    
end


# 2D thermal block PDE model

mutable struct PDE_tbp <: AbstractFOM

    fom::PyObject
    fom_data::Dict{Any, Any}
    sizeSolution::Int64
    nbFOMcalls::Int64
    scoreOption::Int64

    # constructor
    function PDE_tbp(p1_,p2_,diameter_,scoreOption_)
        tbp = pm_basic.thermal_block_problem(num_blocks=(p1_,p2_),parameter_range=(0.001, 1))
        fom_, fom_data_ = pm_basic.discretize_stationary_cg(tbp, diameter = diameter_)
        sizeSolution_=length(fom_.solve(ones(p1_*p2_)).to_numpy())
        new(fom_, fom_data_,sizeSolution_,0,scoreOption_)
    end 
    
end
################################################## ROMs ################################################## 

abstract type AbstractROM end 

#1D analytical spline model

mutable struct SplineApprox <: AbstractROM
    
    xSample::AbstractVector{Float64}
    fFom::AbstractVector{Float64}
    fRom::BSplineManifold
    nbROMcalls::Int64

    # constructors 
    function SplineApprox()
        xs=[]
        fs=[]
        new(xs,fs)
    end
    function SplineApprox(xs_::AbstractVector{Float64},fs_::AbstractVector{Float64})
        p=sortperm(xs_)
        xs=xs_[p]
        fs=fs_[p]
        new(xs,fs,interpolate(xs,fs),0)
    end

end

function interpolate(xs::AbstractVector, fs::AbstractVector{T}) where T
    # Cubic open B-spline space
    p = 3
    k = KnotVector(xs) + KnotVector([xs[1],xs[end]]) * p
    P = BSplineSpace{p}(k)

    # dimensions
    m = length(xs)
    n = dim(P)

    # The interpolant function has a f''=0 property at bounds.
    ddP = BSplineDerivativeSpace{2}(P)
    dda = [bsplinebasis(ddP,j,xs[1]) for j in 1:n]
    ddb = [bsplinebasis(ddP,j,xs[m]) for j in 1:n]

    # Compute the interpolant function (1-dim B-spline manifold)
    M = [bsplinebasis(P,j,xs[i]) for i in 1:m, j in 1:n]
    M = vcat(dda', M, ddb')
    y = vcat(zero(T), fs, zero(T))
    return BSplineManifold(pinv(M,1e-1*eps(eltype(M)))*y, P)
end

function updateROM!(spline::SplineApprox,newSample,newFFom)
    xs=vcat(spline.xSample, newSample)[:,1]
    fs=vcat(spline.fFom, newFFom)[:,1]
    p=sortperm(xs)
    spline.xSample=xs[p]
    spline.fFom=fs[p]
    spline.fRom=interpolate(xs[p],fs[p])
    return true
end


# 2D thermal block PDE RB model

mutable struct RBApprox <: AbstractROM
    
    fomTbp::Ref{PDE_tbp}
    rom::PyObject
    xSample::Matrix{Float64}
    reductor::PyObject
    nbROMcalls::Int64
    
    # constructeurs 
    function RBApprox(fomTbp_::PDE_tbp,x_training::Matrix{Float64},N_samplesInit_)

        training_set = []
        greedy_data = []
        for k=1:length(x_training[:,1])
            push!(training_set,Dict("diffusion" =>  PyObject(x_training[k,:])))
        end 
        reductor_ = pm_basic.CoerciveRBReductor(fomTbp_.fom,
                                                product=fomTbp_.fom.h1_0_semi_product,
                                                coercivity_estimator=pm_basic.ExpressionParameterFunctional("min(diffusion)", fomTbp_.fom.parameters))
        @suppress begin 
            greedy_data =  pm_basic.rb_greedy(fomTbp_.fom, reductor_, training_set, max_extensions=N_samplesInit_) 
        end
        rom_ = greedy_data["rom"]
        xs_=zeros(N_samplesInit_,length(x_training[1,:]))
        for k=1:N_samplesInit_
            xs_[k,:]=greedy_data["max_err_mus"][k]["diffusion"]
        end
        println("Error bounds begining/end greedy algorithm : ",greedy_data["max_errs"][1]," / ",greedy_data["max_errs"][N_samplesInit_])
        new(Ref(fomTbp_),rom_,xs_,reductor_,0)
    end
    function RBApprox(fomTbp::PDE_tbp,rom::PyObject, xSample::Matrix{Float64},reductor::PyObject,nbROMcalls::Int64)
       new(Ref(fomTbp),rom,xSample,reductor,nbROMcalls)
    end
    function RBApprox()
        new()
    end
end

function updateROM!(RB::RBApprox,newSample,newFFom)
    
    
    #println("sample  : ",newSample)
    training_set = []
    greedy_data = []
    push!(training_set,Dict("diffusion" =>  PyObject(newSample)))
    @suppress begin 
        greedy_data =  pm_basic.rb_greedy(RB.fomTbp[].fom, RB.reductor, training_set, max_extensions=1) 
    end
    println("Error bound before ROM update : ",greedy_data["max_errs"][1])
    RB.rom = greedy_data["rom"]
    RB.xSample=[RB.xSample;newSample]
    if greedy_data["max_errs"][1]<=1e-10
        continueUpdate=false
    else
        continueUpdate=true
    end
    return continueUpdate
    # NB:As, the snapshot computed in rb_greedy procedure is not accessible outisde the function
    #    we need to compute an extra time this snapshot 
    #    (precomputed and passed as the useless function argument 'newFFom')
    #    Therefore we do not consider this extra FOM evaluation in the budget and do not increment nbFOMeval
    # fomTbp[].nbFOMeval=fomTbp[].nbFOMeval+1

end


################################################## FOMs & ROMs evaluation ################################################## 


#evaluate 1D analytical model FOM and ROM & error

function evaluateFOM(fom::Fom1D,sample)
   
    m = length(sample)
    y = [1/sample[i]  for i in 1:m ] 
    ztmp=y.>fom.L
    y=@. y*(1-ztmp)+ztmp*fom.L;

    ztmp1=sample.<fom.p1
    mask=[ztmp1[i] && sample[i]>fom.p2 for i in 1:m ];
    y=@. ztmp1*y+mask*(sin(sample-fom.p2)*sin(sample-fom.p2))*fom.p3+(1-ztmp1)*(sin(fom.p1-fom.p2)*sin(fom.p1-fom.p2)-(sample-fom.p1)*0.1)*fom.p3;
    
    fom.nbFOMcalls=fom.nbFOMcalls+m
    
    return max.(y,0)
end

function evaluateROMandError(spline::SplineApprox,sample)
    m = length(sample)
    s=max.([unbounded_mapping(spline.fRom,sample[i])  for i in 1:m],0.)
    e=2 .*(abs.(s-evaluateFOM(fom,sample)))[:,1]
    fom.nbFOMcalls=fom.nbFOMcalls-m # in real situation,  error computed withour resorting to fom !
    spline.nbROMcalls=spline.nbROMcalls+m
    return s,e
end

function evaluateROM(spline::SplineApprox,sample)
    m = length(sample)
    s=max.([unbounded_mapping(spline.fRom,sample[i])  for i in 1:m],0.)
    spline.nbROMcalls=spline.nbROMcalls+m
    return s
end


#evaluate 2D thermal block PDE model FOM and ROM & error

function evaluateFOM(fomTbp::PDE_tbp,samples)
   
        m = length(samples[:,1])
        y=zeros(m)
        #timeCount=0
        @suppress begin
            if fomTbp.scoreOption==1 # mean abs temperature 
                #timeCount=@elapsed  
                y=[mean(abs.(fomTbp.fom.solve(samples[i_snap,:]).to_numpy()))  for i_snap=1:m] 
            else              # max temperature score
                #timeCount=@elapsed  
                y=[maximum(fomTbp.fom.solve(samples[i_snap,:]).to_numpy())  for i_snap=1:m]
            end
        end
        fomTbp.nbFOMcalls=fomTbp.nbFOMcalls+m
        #println("time for solving ",m," foms : ",timeCount)
        return y
    end

function evaluateROM(RBRom::RBApprox,samples)
    m = length(samples[:,1])
    if RBRom.fomTbp[].scoreOption==1 # mean abs temperature 
        y=[mean(abs.(RBRom.reductor.reconstruct(RBRom.rom.solve(samples[ii,:])).to_numpy()))  for ii=1:m]
    else              # max temperature score
        y=[maximum(RBRom.reductor.reconstruct(RBRom.rom.solve(samples[ii,:])).to_numpy())  for ii=1:m]
    end
    
    RBRom.nbROMcalls=RBRom.nbROMcalls+m
    return y
end

function evaluateROMandError(RBRom::RBApprox,samples)
    
    m = length(samples[:,1])
    #pi2 = pi*pi
    # timeCount=0
    # timeCountE=0
    if RBRom.fomTbp[].scoreOption==1 # mean abs temperature 
        #timeCount=@elapsed 
        y=[mean(abs.(RBRom.reductor.reconstruct(RBRom.rom.solve(samples[ii,:])).to_numpy()))  for ii=1:m]
        #timeCountE=@elapsed 
        e=[RBRom.rom.estimate_error(samples[ii,:])[1] for ii=1:m]./pi
    else              # max temperature score
        #timeCount=@elapsed 
        y=[maximum(RBRom.reductor.reconstruct(RBRom.rom.solve(samples[ii,:])).to_numpy())  for ii=1:m]
        #timeCountE=@elapsed 
        e=[RBRom.rom.estimate_error(samples[ii,:])[1] for ii=1:m]
    end
        
    
    RBRom.nbROMcalls=RBRom.nbROMcalls+m
    # println("time for solving / computing error bound of ",m," roms : ",timeCount," / ",timeCountE)
    return y,e 
end

################################################## ARMS parameters ################################################## 

mutable struct ARMS_parameters

    
    bridge::Bool
    budget::Int64
    N_samples::Int64
    proportionKilling::Float64
    logCost::Float64
    miniNumberHitsForIS
    epsilonStopUpdate::Float64
    refLaw::AbstractRefDistrib
    alphaProp_init::Float64
    kernelParam::OrsteinUhlenbeckParams
    meanScore::Int64
    L_max::Float64
    rng
    fom::AbstractFOM

    function ARMS_parameters(fom_,bridge_,budget_,N_samples_,proportionKilling_,logCost_,miniNumberHitsForIS_,epsilonStopUpdate_,refLaw_,alphaProp_init_,kernelParam_,meanScore_,L_max_,rng_)
        new(bridge_,budget_,N_samples_,proportionKilling_,logCost_,miniNumberHitsForIS_,epsilonStopUpdate_,refLaw_,alphaProp_init_,kernelParam_,meanScore_,L_max_,rng_,fom_)
    end
end

function saveInputParameters(filePath,param,rom_inital)
    open(filePath, "w") do io
      println(io," ARMS parameters= ",param)
      println(io," rom initial=", rom_inital)
    end
end