
#Written By Patrick Héas.                                          
#Copyright 2024 INRIA. All rights reserved  

# Main algorithm: Adaptive reduced multilevel splitting (ARMS)

function ARMS(romInit,param)

    N_samples=param.N_samples
    budget=param.budget
    logCost=param.logCost
    miniNumberHitsForIS=param.miniNumberHitsForIS

    #############
    #Initialization
    #############
     
    samples=refSampler(param.refLaw,N_samples,param.rng)
    Z=[[1.]]
    L=[[-Inf64]]
    roms=[]
    samplesHistory=[copy(samples)]
    push!(Z,[1.])
    push!(L,[-Inf64])
    param.kernelParam.alpha_prop=param.alphaProp_init
    push!(roms,romInit); push!(roms,deepcopy(romInit))
    maxSampling=true
    NumberHits=0
    FirstIndexForIS=-1
    IS_estimates_pointwise=zeros(budget)
    IS_estimate=zeros(budget)
    IS_validPoints=ones(budget)
    SMC_estimate=zeros(budget)
    nbROMeval=zeros(budget)
    nbFOMeval=zeros(budget)
    param.fom.nbFOMcalls=0
    
    #############
    # AMS Loop
    #############
    logCostLevel=logCost
    continueROMUpdate=true
    ROMconvergence=0

    ##### untill the snapshot budget is reached or ROM sufficiently accurate ##### 
    it=1
    maxHistory=5 # maximum number of back steps in sample/rom history
    depthPastInclusion=5 # maximum number of previous samples in history tested for inclusion 
    maxNumberRomConvergenceCriterion=2 #if epsilon =0, then max number of times the stopping criterion for ROM convergence has to be met

    while (it<budget) &&  continueROMUpdate
        it=it+1
        println("#################### ARMS iteration ",it-1," ####################")
        if ROMconvergence>maxNumberRomConvergenceCriterion && epsilonStopUpdate>=0 && NumberHits> miniNumberHitsForIS# to exit while loop at the next iteration 
            continueROMUpdate=false
        end 
        # adaptive increase of levels (cte normalization factor & entropic criterion)
        sRom,eRom,conv=SMC_increaseL!(roms[it],samples,Z[it],L[it],logCostLevel,param)
        if (conv) ROMconvergence=ROMconvergence+1 end
        push!(samplesHistory,copy(samples))
        push!(L,[L[it][end]])
        push!(Z,[Z[it][end]])

       # sampling with learning function (sampling uniformly & kernel mixing or max error)
        if (!maxSampling)
            snapshotSample=samples[ceil.(Int64,rand(param.rng,1)*N_samples),:]
            mutate!(roms[it],snapshotSample,L[it][end],param)
        else
            snapshotSample=samples[sortperm(eRom)[N_samples],:]';
        end
        
       # updating the ROM 
       push!(roms,deepcopy(roms[it]))
       snapshot=evaluateFOM(param.fom,snapshotSample) 
       println("sample / snapshot: ",snapshotSample,"/",snapshot)
       if continueROMUpdate 
            updateROM!(roms[it+1],snapshotSample,snapshot)
       else
            println("-------------------------------------------- end of ROM updates ...")
       end
       if (it-1-max(maxHistory,maxHistory)>0) # releaving unecessary memory 
            roms[it-1-max(maxHistory,maxHistory)]=RBApprox() 
            GC.gc(); 
       end
       #saveAMSstate(roms[it],it)

       # learning function update & setting first index for IS estimator 
       if (snapshot[end]>=param.L_max)
            NumberHits+=1
            if (NumberHits> miniNumberHitsForIS && FirstIndexForIS==-1)
                println("-------------------------------------------- swithcing learning function to random sampling ... ")
                maxSampling=false
                FirstIndexForIS=it  
            end
       end 
        
       # importance sampling estimate at snapshot sample (performed only if worst case domination) & SMC estimate
        if  (FirstIndexForIS>0)
           IS_estimates_pointwise[it]=Z[it+1][end]*Float64(snapshot[end] >= param.L_max)
           SMC_estimate[it]=Z[it+1][end]*mean(Float64.(sRom .>=param.L_max))
           println("IS point estimate = ", IS_estimates_pointwise[it], ", SMC estimate = ",SMC_estimate[it])
           if (!maxSampling && logCostLevel <Inf64 )
                logCostLevel=Inf64
                println("-------------------------------------------- end of entropic criterion ...  ")
           end
        else
           IS_validPoints[it]=0
           SMC_estimate[it]=Z[it+1][end]*mean(Float64.(sRom .>=param.L_max))
           println("SMC estimate = ",SMC_estimate[it])
        end

       # bridge AMSs or decide to restart from the beginning the next AMS simulation 
       if bridge #&& !( ROMconvergence>maxNumberRomConvergenceCriterion && epsilonStopUpdate>=0 )
        if continueROMUpdate
            # guaranteeing nested sets, entropic and quantile criteria
            bridging!(Z,L,samplesHistory,samples,sRom,eRom,it,roms,logCostLevel,maxHistory,depthPastInclusion,param)
        end
       else
        if continueROMUpdate
            # restart simulation 
            samples.=refSampler(param.refLaw,N_samples,param.rng)
            Z[it+1][end]=1
            L[it+1][end]=-Inf64
            param.kernelParam.alpha_prop=param.alphaProp_init
        end
       end

       nbROMeval[it]=roms[it].nbROMcalls
       nbFOMeval[it]=param.fom.nbFOMcalls
    end

    #####  IS for remaining snapshot budget (if bridge else no additional IS) ##### 
    if bridge
      for itLeft=it+1:budget
        snapshotSample=samples[ceil.(Int64,rand(param.rng,1)*N_samples),:]
        snapshot=evaluateFOM(param.fom,snapshotSample) 
        IS_estimates_pointwise[itLeft]=Z[it+1][end]*Float64(snapshot[end] >= param.L_max)
        SMC_estimate[itLeft]=SMC_estimate[it]
        nbROMeval[itLeft]=roms[it].nbROMcalls
        nbFOMeval[itLeft]=param.fom.nbFOMcalls
      end
    else
      for itLeft=it+1:budget
        IS_estimates_pointwise[itLeft]=0
        SMC_estimate[itLeft]=SMC_estimate[it]
        nbROMeval[itLeft]=roms[it].nbROMcalls
        nbFOMeval[itLeft]=param.fom.nbFOMcalls
      end
    end
    
    ##### Prepare IS estimates outputs ##### 

    #gather IS estimates from FirstIndexForIS 
    if bridge
        if (FirstIndexForIS>0 && FirstIndexForIS+1<=length(IS_estimate) )
            IS_estimate[FirstIndexForIS+1:length(IS_estimate)].=[sum(IS_estimates_pointwise[FirstIndexForIS+1:k])/sum(IS_validPoints[FirstIndexForIS+1:k])  for k in FirstIndexForIS+1:length(IS_estimate)]    
        end
    else
        if (FirstIndexForIS>0 && FirstIndexForIS+1<=length(IS_estimate) )
            IS_estimate[FirstIndexForIS+1:it].=[sum(IS_estimates_pointwise[FirstIndexForIS+1:k])/sum(IS_validPoints[FirstIndexForIS+1:k])  for k in FirstIndexForIS+1:it]
            IS_estimate[it+1:end].=IS_estimate[it]
        end
    end
    # exclude case where IS_estimate undefined (points from FirstIndexForIS+1 not valid points)
    for k in eachindex(IS_estimate)  
        if isnan(IS_estimate[k])
            IS_estimate[k]=0
        end
    end
    println("***** End run: IS estimate = ", IS_estimate[end], ", SMC estimate = ",SMC_estimate[end]," *****")
    
   return  IS_estimate, SMC_estimate,IS_estimates_pointwise, nbROMeval, nbFOMeval

end     