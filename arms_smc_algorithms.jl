#Written By Patrick Héas.                                          
#Copyright 2024 INRIA. All rights reserved  

function SMC_increaseL!(rom_current,samples,Z,L,logCostLevel,param) 
    
    maxIt=400
    m=size(samples,1)
    N_tresh=Int(floor(m*param.proportionKilling))
    
    # Ordered Init rom_current samples, scores and errors
    sRom,eRom=evaluateROMandError(rom_current,samples)
    sRomIndex=sortperm(sRom);
    samples.=samples[sRomIndex,:];sRom.=sRom[sRomIndex]; eRom.=eRom[sRomIndex]

    #Initialize loop 
    i=1
    worstLogCost=0
    Ltry=-Inf64
    numberDominatingSamples=m

    #Loop
    while (worstLogCost<=logCostLevel) && numberDominatingSamples==m && (i<=maxIt)  && (Ltry<param.L_max) 
        k=0
        if sRom[N_tresh]< param.L_max 
          while sRom[N_tresh]==sRom[min(N_tresh+1+k,length(sRom))] && N_tresh+1+k<length(sRom)
            k=k+1 # adjusts k to kill all particles below or equal sRom[N_tresh]
          end
        end
        Ltry=min(sRom[N_tresh+k],param.L_max ) 
        #entropic criterion
        pL=sum(Int32.(sRom.> Ltry))/m # NB: different of pL=1-(N_tresh+k)/m only at the last  level 
        pLplusE=sum(Int32.(sRom .- eRom .> Ltry))/m    
        worstLogCost=log(pL/pLplusE)
        #domination criterion
        support_Ltry=Int32.(sRom.-Ltry .> 0.)
        support_Optimistic_Lmax=Int32.(sRom.+eRom.-param.L_max .> 0.)
        numberDominatingSamples=sum(Int32.((support_Ltry.- support_Optimistic_Lmax) .>= 0 ))
        if ( worstLogCost<=logCostLevel && numberDominatingSamples==m && Ltry<param.L_max)
            #accept increase
            push!(L,Ltry)
            #update nomalization
            push!(Z,Z[i]*pL)
            #resample mutate
            resampleMutate!(rom_current,samples,N_tresh+k,L[i+1],param)
            # ressort
            sRom[1:N_tresh+k],eRom[1:N_tresh+k]=evaluateROMandError(rom_current,samples[1:N_tresh+k,:])
            sRomIndex=sortperm(sRom);
            samples.=samples[sRomIndex,:];sRom.=sRom[sRomIndex]; eRom.=eRom[sRomIndex]
            i+=1
            println("--------------> level ",Ltry, ", nb eval ROM ", rom_current.nbROMcalls); 
        end
        
    end
    worstLogCostLtry=worstLogCost
    worstLogCost=log(sum(Int32.(sRom.> L[end]))/sum(Int32.(sRom .- eRom .> L[end])))
   
    println("L^(k)=",L[end]," WLC=",worstLogCost," Z^(k)=",Z[end],", (Ltry=",Ltry," WLC try =",worstLogCostLtry," nb samples dominating=",numberDominatingSamples, ", nb eval ROM ", rom_current.nbROMcalls," nb level increase=", i-1,")")
    RomConvergence=false
    # to stop ROM updating if ROM is accurate near l_max (domination is fullfiled before or after exiting loop)
    if worstLogCost<=param.epsilonStopUpdate   && Ltry>= param.L_max*0.98 #&& numberDominatingSamples==m
        RomConvergence=true
    end
    return sRom,eRom,RomConvergence

end

function resampleMutate!(rom_current,samples,N_tresh,Ltresh,param)
                
    #Uniform sampling in [Ntresh+1:N_samples]
    idx=ceil.(Int64,rand(param.rng,N_tresh)*(param.N_samples-N_tresh)).+N_tresh
    subSample=copy(samples[idx,:])
  
    prop=copy(subSample)
    sRom_prop=ones(N_tresh)
    acceptProp=ones(N_tresh)

    #Metropolis kernel application
     for imove=1:kernelParam.nMove
        
        if param.refLaw.name=="logNormalLaw"
            # change to std Normal coordinates:
            prop .= (log.(subSample).-param.refLaw.mu_ref) ./ param.refLaw.sigma_ref ;
        end
        # state move Orstein-Uhlenbeck:
        prop .= 1. ./sqrt(1. .+ param.kernelParam.alpha_prop^2) .* ( prop .+ param.kernelParam.alpha_prop .* randn(param.rng,Float64,(N_tresh,param.refLaw.paramDim)));
       
        if param.refLaw.name=="logNormalLaw"
            # back to log Normal:
            prop .= exp.(prop .* param.refLaw.sigma_ref .+ param.refLaw.mu_ref);
        end
        sRom_prop .=evaluateROM(rom_current,prop)
        acceptProp .= Float64.(sRom_prop .> Ltresh)
        subSample .= acceptProp .* prop .+ (1. .- acceptProp) .* subSample
        
        #adpat Orstein-Uhlenbeck kernel to acceptance rate 
        # [Garthwaite, Paul H., Yanan Fan, and Scott A. Sisson.
        #  Adaptive optimal scaling of Metropolis–Hastings algorithms using the Robbins–Monro process." 
        #  Communications in Statistics-Theory and Methods 45.17 (2016): 5098-5111.]  

        if param.refLaw.name=="logNormalLaw"   
            param.kernelParam.alpha_prop=exp(log(param.kernelParam.alpha_prop)+ param.kernelParam.c*(mean(acceptProp)-param.kernelParam.pstar)/Float64(imove));
        else
            param.kernelParam.alpha_prop=param.kernelParam.alpha_prop+ param.kernelParam.c*(mean(acceptProp)-param.kernelParam.pstar)/Float64(imove);           
        end
        
    end
    samples[1:N_tresh,:].=subSample
   
end

function mutate!(rom_current,subSample,Ltresh,param)
            
    prop=copy(subSample)
    sRom_prop=ones(size(subSample,1))
    acceptProp=ones(size(subSample,1))
    m=size(prop,1)

    #Metropolis kernel application (no kernel adaptation)
     for imove=1:param.kernelParam.nMove
        
        if param.refLaw.name=="logNormalLaw"
            # change to std Normal coordinates:
            prop .= (log.(subSample).-param.refLaw.mu_ref) ./ param.refLaw.sigma_ref ;
        end
        # state move Orstein-Uhlenbeck:
        prop .= 1. ./sqrt(1. .+ param.kernelParam.alpha_prop^2) .* ( prop .+ param.kernelParam.alpha_prop .* randn(param.rng,Float64,(m,param.refLaw.paramDim)));
       
        if param.refLaw.name=="logNormalLaw"
            # back to log Normal:
            prop .= exp.(prop .* param.refLaw.sigma_ref .+ param.refLaw.mu_ref);
        end
        sRom_prop=evaluateROM(rom_current,prop)
        acceptProp = Float64.(sRom_prop .> Ltresh)
        subSample .= acceptProp .* prop .+ (1. .- acceptProp) .*subSample
       end

        
end


function bridging!(Z,L,samplesHistory,samples,sRom,eRom,it,roms,logCostLevel,maxHistory,depthPastInclusion,param)

       # -> search tilde k and level l_0(tilde k) satisfying worst-case inclusion and quantile condition   
       inclusion=false
       worstLogCost=Inf64
       N_tresh=Int(floor(param.N_samples*param.proportionKilling))
       N_tresh_next=N_tresh
       kprim=it+1
       L0Kprim=-Inf64
       sRom_kprim=sRom
       eRom_kprim=eRom
       sRom_next=copy(sRom) # arbitrary for initialization 
       eRom_next=copy(eRom) 
       while ((!inclusion || worstLogCost > logCostLevel) && kprim>1 && it-kprim<maxHistory)
           kprim=kprim-1

           # previous and next reduced scores and errors using previous N-sample
           if (kprim<it)
                # corresponds to past scores and errors
                sRom_kprim,eRom_kprim=evaluateROMandError(roms[kprim],samplesHistory[kprim])
           end
           sRom_next,eRom_next=evaluateROMandError(roms[it+1],samplesHistory[kprim])

           #order previous samples according to updated score
           sRomIndex=sortperm(sRom_next);
           samples.=samplesHistory[kprim][sRomIndex,:];sRom_next.=sRom_next[sRomIndex]; eRom_next.=eRom_next[sRomIndex]

           # obtain tilde M quantile below l_max*0.99 
           delta_lmax=0
           L0Kprim=sRom_next[N_tresh] # \tilde M = M
           while L0Kprim >  L[it][end]*0.9 && N_tresh+delta_lmax>1 
                delta_lmax=delta_lmax-1
                L0Kprim=sRom_next[N_tresh+delta_lmax]  # lowering \tilde M to be lower  l_max
           end

           # check the worst log cost constraint & domination constraint and lower L0Kprim if necessary 
           # worst log cost constraint
           support_Ltry=Int32.(sRom_next.-L0Kprim .> 0.)
           pL=sum(support_Ltry) 
           pLplusE=sum(Int32.(sRom_next .- eRom_next .> L0Kprim))    
           worstLogCost=log(pL/pLplusE)
           #domination constraint
           support_Optimistic_Lmax=Int32.(sRom_next.+eRom_next.-param.L_max .> 0.)
           numberDominatingSamples=sum(Int32.((support_Ltry.- support_Optimistic_Lmax) .>= 0 ))
           delta_wlc=0
           while (worstLogCost > logCostLevel && param.N_samples > numberDominatingSamples && N_tresh+delta_lmax+delta_wlc>1)
                delta_wlc=delta_wlc-1
                L0Kprim=sRom_next[N_tresh+delta_lmax+delta_wlc] # lowering \tilde M to meet the wlc & domination condition 
                support_Ltry=Int32.(sRom_next.-L0Kprim .> 0.)
                pL=sum(support_Ltry) 
                pLplusE=sum(Int32.(sRom_next .- eRom_next .> L0Kprim))    
                worstLogCost=log(pL/pLplusE)     
                numberDominatingSamples=sum(Int32.((support_Ltry.- support_Optimistic_Lmax) .>= 0 ))
           end
        
           # if l0Kprim satisfying quantile minimum condition exists
           # and worst log cost condition & dominating condition satisfied (where L0Kprim is not lowered too much to meet these last conditions)
           if (worstLogCost <= logCostLevel && ~(delta_lmax==0 && abs(delta_wlc)/param.N_samples> param.proportionKilling*2/3. ) &&  numberDominatingSamples==param.N_samples) 
                # adjusts delta_k to kill all particles below L0Kprim (increasing \tilde M) 
                delta_k=0
                while (      sRom_next[N_tresh+delta_lmax+delta_wlc]==sRom_next[min(N_tresh+1+delta_lmax+delta_wlc+delta_k,length(sRom_next))] 
                        &&     N_tresh+1+delta_lmax+delta_wlc+delta_k<length(sRom_next)    )
                        delta_k=delta_k+1 
                end
                N_tresh_next=N_tresh+delta_lmax+delta_wlc+delta_k
                L0Kprim=sRom_next[N_tresh_next]
                L0Kprim=min(L0Kprim,param.L_max) # if ever all particles are above l_max 

                # check inclusion for samples[j] with j<kprim of the set {new score sRom_next above l0kprim} in the set {previous scores sRomHistory[j] above l[j]}
                inclusion=true
                j=kprim-2
                while (j>0 && L[j+1][1]>0 && inclusion && kprim-2-j< depthPastInclusion )
                    sRom_kprim_onSetj=evaluateROM(roms[kprim],samplesHistory[j])
                    support_sRomkprim_Lkprim=Int32.(sRom_kprim_onSetj .> L[kprim][end])
                    sRom_next_onSetj=evaluateROM(roms[it+1],samplesHistory[j])
                    support_sRomknextL0Kprim=Int32.(sRom_next_onSetj .> L0Kprim )
                    numberIncludedSamples=sum(Int32.((support_sRomkprim_Lkprim.- support_sRomknextL0Kprim) .>= 0 ))
                    inclusion=inclusion && (numberIncludedSamples==param.N_samples) 
                    if (~inclusion)
                        println("j= ",j," diff L=",L0Kprim-L[kprim][end]," L[kprim][end]= ",L[kprim][end]," L0Kprim =",L0Kprim," numberIncludedSamples= ",numberIncludedSamples," supp B=",sum(support_sRomkprim_Lkprim)," supp A=",sum(support_sRomknextL0Kprim))
                    end
                    j=j-1
                end 
           end
       end
       println("kprim=",kprim-1, ", Lkprim= ", L[kprim][end]) 
       
        if (!inclusion) 
            println("no distribution to bridge with next ROM in the previous ones ... ") 
        else
            println("previous rom index for bridging ",kprim-1, ", nb of steps back ",it-kprim, ", lo(kprim)=",L0Kprim) 
        end
       
       # -> compute the normalization correction, or prepare to restart SMC simulation
       if (kprim>1 && it-kprim<maxHistory) 
            # normalization update
            Zfactor=sum(sRom_next.>L0Kprim)/param.N_samples
            Z[it+1][end]=Zfactor*Z[kprim][end]
            L[it+1][end]=L0Kprim
            println("correction -> Z0(kprim): ",Z[it+1][end],"=",Z[kprim][end],"x",Zfactor," , L0(kprim)=",L0Kprim, " (kprim=",kprim-1,")") 
            #resample mutate
            resampleMutate!(roms[it+1],samples,N_tresh_next,L0Kprim,param)
           
        else # else re-intialize and prepare to restart simulation  from beginning
            println("restart beginning -> Z=-inf") 
            Z[it+1][end]=1.
            L[it+1][end]=-Inf64
            samples.=refSampler(param.refLaw,param.N_samples,param.rng)
            param.kernelParam.alpha_prop=alphaProp_init
        end
end



