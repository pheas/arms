#Written By Patrick Héas.                                          
#Copyright 2024 INRIA. All rights reserved  

#####################################################################################
#####################################################################################
#       This demo implements rare event simulation,
#       where the score function is the L_p norm of a parametric PDE solution approximated by reduced basis. 
#       The chosen PDE corresponds to the well-studied thermal block problem, which models heat diffusion in an heterogeneous media. 
#       The PDE is parametrized by a random vector of diffusion coefficients.
#       The reference distribution for the random vector components are independent log-normal distributions. 
#       Details on the model can be found in in the paper  
#      `Adaptive Reduced Multilevel Splitting' by F. Cérou, P. Héas and M. Rousset.   
#####################################################################################
#####################################################################################

include("arms_librairies.jl")
include("arms_main_algorithm.jl")
include("arms_models_and_parameters.jl")
include("arms_smc_algorithms.jl")
include("arms_plots.jl")

println("\n************************************************************************************* ")
println("*********************** Adaptive reduced multilevel splitting demo ****************** ")
println("*************************************************************************************\n ")

#####################################################################################
#----------------------------- Parameter setting -----------------------------------#
#####################################################################################

#################
## output path
#################

rootpath="./test/"
extpath="arms_RBdemo"

#################
## ARMS parameters
#################
# true if bridging AMS simulations
bridge=true
# budget K 
budget=200
# number of snapshots to build initial reduced score
N_samplesInit=5
# number of particles N
N_samples=400
# proportion of killing M/N
proportionKilling=0.3
#worst logcost threshold 
logCost=0.05 
#number of hits J before Importance Sampling trigerred  
miniNumberHitsForIS=2
# if stop reduced score update if sufficiently accurate (epsilonStopUpdate >= 0) else (epsilonStopUpdate <0)
epsilonStopUpdate=logCost/10
#Markovian kernel 
alphaProp_init=0.3 
kernelParam=OrsteinUhlenbeckParams(alphaProp_init,0.5,0.09,30)
#Type of L_p norm for the score  
meanScore=1 # 1 if temperature mean (p=1) or !=1 if temperature max (p=infinity) 
# maximum level of interest
L_max=0.5

#################
## PDE and RB parameters
#################
nbBlocksX=2
nbBlocksY=2
diameter=0.02
dimParameter=nbBlocksX*nbBlocksY

#################
#ref distribution parameters
#################
seed=1233
rng = MersenneTwister(seed)
refLaw=logNormal(dimParameter,0.6,0.8)

#####################################################################################
#----------------------------- Initialization --------------------------------------#
#####################################################################################
println("\n*********************** Initialization ***********************\n ")
println("---------> Building thermal block FOM ...")
fom=@suppress begin PDE_tbp(nbBlocksX,nbBlocksY,diameter,meanScore) end
println("FOM system of dimension = ",fom.sizeSolution)

println("---------> Building thermal block initial RB ...")
initSamples=refSampler(refLaw,N_samplesInit*10,rng)
rom_inital=@suppress begin RBApprox(fom,initSamples,N_samplesInit) end

#####################################################################################
#----------------------------- Running ARMS    -------------------------------------#
#####################################################################################
println("\n*********************** Running ARMS  ***********************\n ")
println("---------> Lauching ARMS ...")
#creating ART param object and saving it
param=ARMS_parameters(fom,bridge,budget,N_samples,proportionKilling,logCost,miniNumberHitsForIS,epsilonStopUpdate,refLaw,alphaProp_init,kernelParam,meanScore,L_max,rng)
saveInputParameters(rootpath*extpath*"_parameters.txt",param,rom_inital)
#running ARMS 
timing = @elapsed p_IS,p_SMC,p_IS_pointwise, nbROMcalls, nbFOMcalls=ARMS(rom_inital,param)
println(" -> proba IS = ", p_IS[end], ", proba SMC = ", p_SMC[end], ", timing=",timing,", nbROMcalls=", nbROMcalls[end], ", nbFOMcalls=", nbFOMcalls[end])

#####################################################################################
#----------------------------- Results saving and display   ------------------------#
#####################################################################################
println("\n*********************** Result saving ***********************\n ")
#saving ARMS outputs
println("saving output data ---------> ",rootpath*extpath*"_outputs.jld")
save(rootpath*extpath*"_outputs.jld", "p_IS_pointwise", p_IS_pointwise , "p_IS", p_IS, "p_SMC", p_SMC, "nbROMcalls", nbROMcalls, "nbFOMcalls", nbFOMcalls, "timing", timing)
# results visualization
p_True=0.0006072672165436441
RomGain=4.5e-2
p00,p01=displayResults(p_True,p_IS,p_SMC,budget,nbROMcalls,nbFOMcalls,RomGain,1)
println("saving plot ---------> ",rootpath*extpath*"_mean_wrt_iteration.pdf")
savefig(p00,rootpath*extpath*"_mean_wrt_iteration.pdf") 
println("saving plot ---------> ",rootpath*extpath*"_RMSEvsCost_wrt_iteration.pdf")
savefig(p01,rootpath*extpath*"_RMSEvsCost_wrt_iteration.pdf") 

println("\n*********************** End of ARMS demo  ***********************\n ")
