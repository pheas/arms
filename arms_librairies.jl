
using JLD
using Plots
using Random
using SpecialFunctions
using StaticArrays
using LinearAlgebra
using LaTeXStrings
using BasicBSpline
using PyCall
using SharedArrays
using Statistics
using Suppressor
pm_basic = pyimport_conda("pymor.basic", "pymor")
pickle = pyimport("pickle")
