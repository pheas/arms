#  Adaptive Reduced Multilevel Splitting algorithm
Written By Patrick Héas.                                          
Copyright 2024 INRIA.
GNU Affero General Public License, AGPL-3.0-or-later

## Descritipion 
This code implements the algorithm presented in the paper  `Adaptive Reduced Multilevel Splitting' by F. Cérou, P. Héas and M. Rousset. By the use of reduced order models, this algorithm lowers the computational cost of rare event simulation when the score function is very expensive to evaluate.

The code is written in the Julia language: https://docs.julialang.org

## Dependencies: Python, pyMor library and Julia packages

ARMS requires the Julia packages appearing in the Project.toml file. They can be installed typing in a Julia terminal:  

    ]
    instantiate 

In this demo, ARMS requires the installation of Python programming language https://www.python.org/, as it uses routines of the pymor libraires https://github.com/pymor/pymor for solving paramteric PDEs and reduced modeling with reduced basis techniques. The python routines are interfaced with julia thanks to the pyCall Julia package.

## Runing the demo 

The demo implements rare event simulation,
        where the score function is the L_p norm of a parametric PDE solution approximated by reduced basis. 
        The chosen PDE corresponds to the well-studied thermal block problem, which models heat diffusion in an heterogeneous media. 
        The PDE is parametrized by a random vector of diffusion coefficients.
        The reference distribution for the random vector components are independent log-normal distributions.
    
Create the directory for results

    mkdir test

Launch in a Julia REPL terminal (default input parameters can be modified at the beginning of the file)
 
    arms_runRBdemo.jl
    


ARMS's estimates and its cost are saved in the Julia binary file 

    'test/arms_RBdemo_outputs.jld' 

The visualization of ARMS's estimates over the iterations of the algorithm, as well as the graphs of MSE versus cost, are saved in the files 

    'test/arms_RBdemo_mean_wrt_iteration.pdf' 

and 

    'test/arms_RBdemo_RMSEvsCost_wrt_iteration.pdf' 

## Code  brief description

-The main of ARMS code is in 'arms_main_algorithm.jl' 

-The model (evaluation of the score / reduced score model and error, sampling procedure for the reference distribution, etc) are coded in 'arms_model_and_parameters.jl'

-The subroutines subroutines for increasing level until crtical one and bridging are in 'arms_smc_algorithms.jl'

-The routines for display and saving are in 'arms_plots.jl'

-The pymor library is imported via conda in "art_librairies.jl"

